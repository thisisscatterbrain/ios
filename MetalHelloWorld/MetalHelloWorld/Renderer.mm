//
//  Renderer.m
//  SpinningCube
//
//  Created by Arno Luebke on 6/23/15.
//  Copyright (c) 2015 Arno Luebke. All rights reserved.
//

#import "Renderer.h"
#import <simd/simd.h>

@implementation Renderer
{
    id <MTLDevice> _device;
	id <MTLCommandQueue> _commandQueue;
	id <MTLLibrary> _shaderLibrary;
	id <MTLRenderPipelineState> _pipelineState;
	id <MTLBuffer> _positions;
	id <MTLBuffer> _colors;
}

- (void)configure:(View *)view
{
	_device = view.device;
	_commandQueue = [_device newCommandQueue];
	_shaderLibrary = [_device newDefaultLibrary];
	
	if (!_shaderLibrary) {
		NSLog(@"Failed to created shader library");
		assert(0);
		return;
	}
	
	MTLRenderPipelineDescriptor* pipeDesc = [MTLRenderPipelineDescriptor new];
	pipeDesc.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm;
	pipeDesc.vertexFunction = [_shaderLibrary newFunctionWithName:@"vertexShader"];
	pipeDesc.fragmentFunction = [_shaderLibrary newFunctionWithName:@"fragmentShader"];
	
	NSError* err =[NSError new];
	_pipelineState = [_device newRenderPipelineStateWithDescriptor:pipeDesc
		error: &err];
	
	static float vertexData[] = {
		-0.5, -0.5,
		+0.5, -0.5,
		+0.0, +0.5
	};
	
	_positions = [_device newBufferWithBytes:vertexData length:sizeof(vertexData)
		options:0];

	static float colorData[] = {
		0.0, 0.0, 1.0,
		0.0, 1.0, 0.0,
		1.0, 0.0, 0.0
	};
	
	NSLog(@"%lu", sizeof(colorData));
	
	_colors = [_device newBufferWithBytes:colorData length:sizeof(colorData)
		options:0];
}

- (void)render:(View *)view
{
	id <MTLCommandBuffer> cmdBuf = [_commandQueue commandBuffer];
	MTLRenderPassDescriptor* desc = view.renderPassDescriptor;
	
	id <MTLRenderCommandEncoder> enc = [cmdBuf renderCommandEncoderWithDescriptor:desc];
	[enc setRenderPipelineState:_pipelineState];
	[enc setVertexBuffer:_positions offset:0 atIndex:0];
	[enc setVertexBuffer:_colors offset:0 atIndex:1];
	[enc drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:3];
	[enc endEncoding];
	
	[cmdBuf presentDrawable: view.drawable];
	[cmdBuf commit];
}

@end
