//
//  ViewController.m
//  SpinningCube
//
//  Created by Arno Luebke on 6/23/15.
//  Copyright (c) 2015 Arno Luebke. All rights reserved.
//

#import "View.h"
#import "ViewController.h"
#import "Renderer.h"

@implementation ViewController
{
    CADisplayLink *_timer;
	Renderer* _renderer;
}

- (void)initCommon
{
    _renderer = [Renderer new];
}

- (id)init
{
    self = [super init];
    
    if(self) {
        [self initCommon];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil
                           bundle:nibBundleOrNil];
    
    if(self) {
        [self initCommon];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    if(self) {
        [self initCommon];
    }
    
    return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];

	View* v = (View*)self.view;
	v.delegate = _renderer;
	[_renderer configure:v];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	[self dispatchGameLoop];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
	[self stopGameLoop];
}

- (void)dispatchGameLoop
{
    _timer = [[UIScreen mainScreen] displayLinkWithTarget:self
                                                 selector:@selector(gameloop)];
    _timer.frameInterval = 1;
    [_timer addToRunLoop:[NSRunLoop mainRunLoop]
                 forMode:NSDefaultRunLoopMode];
}

- (void)gameloop
{
	View* v = (View *)self.view;
	assert([self.view isKindOfClass:[View class]]);
	[v display];
}

- (void)stopGameLoop
{
    if(_timer)
        [_timer invalidate];
}





@end
