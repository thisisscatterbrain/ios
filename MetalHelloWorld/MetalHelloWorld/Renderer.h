//
//  Renderer.h
//  SpinningCube
//
//  Created by Arno Luebke on 6/23/15.
//  Copyright (c) 2015 Arno Luebke. All rights reserved.
//

#import "View.h"

@interface Renderer : NSObject <ViewDelegate>
- (void)configure:(View *)view;
@end