//
//  Shader.metal
//  SpinningCube
//
//  Created by Arno Luebke on 6/23/15.
//  Copyright (c) 2015 Arno Luebke. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct VertexOut {
	float4 position [[position]];
	float3 color [[user(color)]];
};

vertex VertexOut vertexShader(constant float2* positions [[buffer(0)]],
	constant packed_float3* colors [[buffer(1)]], unsigned int vertexId [[vertex_id]])
{
	VertexOut out;
	out.position = float4(positions[vertexId], 0.0, 1.0);
	out.color = colors[vertexId];
	return out;
}

fragment float4 fragmentShader(VertexOut interpolated [[stage_in]])
{
	return float4(interpolated.color, 1.0);
}
