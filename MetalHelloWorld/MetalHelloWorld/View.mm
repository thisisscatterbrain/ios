//
//  View.m
//  SpinningCube
//
//  Created by Arno Luebke on 6/23/15.
//  Copyright (c) 2015 Arno Luebke. All rights reserved.
//

#import "View.h"

@implementation View
{
    __weak CAMetalLayer *_metalLayer;
}
@synthesize drawable = _drawable;
@synthesize renderPassDescriptor = _renderPassDescriptor;

- (void)initCommon
{
	_renderPassDescriptor = nil;
	_metalLayer = (CAMetalLayer *)self.layer;
	_device = MTLCreateSystemDefaultDevice();
	_metalLayer.device = _device;
    _metalLayer.pixelFormat = MTLPixelFormatBGRA8Unorm;
}

+ (id)layerClass
{
	return [CAMetalLayer class];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

	if (self) {
		[self initCommon];
	}

    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];

	if (self) {
		[self initCommon];
	}

    return self;
}
- (void)setupRenderPassDescriptorForTexture:(id <MTLTexture>) texture
{
    if (_renderPassDescriptor == nil)
		_renderPassDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
	
	_renderPassDescriptor.colorAttachments[0].texture = texture;
	_renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
	_renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(1.0, 1.0, 0.0, 1.0);
}

- (MTLRenderPassDescriptor *)renderPassDescriptor
{
    id <CAMetalDrawable> drawable = self.drawable;

    if(!drawable) {
        _renderPassDescriptor = nil;
    } else {
       [self setupRenderPassDescriptorForTexture: drawable.texture];
    }
    
    return _renderPassDescriptor;
}

- (id <CAMetalDrawable>)drawable
{
	if (_drawable == nil)
		_drawable = [_metalLayer nextDrawable];
    return _drawable;
}

- (void)display
{
	[self.delegate render:self];
	
	// Make sure to get the next drawable for the following frame (see
	// "drawable" implementation).
	_drawable = nil;
}

@end