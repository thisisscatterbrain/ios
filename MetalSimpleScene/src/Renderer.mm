#import "Renderer.h"
#import "RenderContext.h"
#import "Scene.h"
#import <simd/simd.h>

@implementation Renderer
{
	id<MTLDevice>			_device;
	id<MTLCommandQueue>		_commandQueue;
	id<MTLLibrary>			_shaderLibrary;
	id<MTLRenderPipelineState>	_pipelineState;
	Scene*				_scene;
}

- (void)configure:(View *)view
{
	_device = [[RenderContext instance] device];
	_commandQueue = [_device newCommandQueue];
	_shaderLibrary = [_device newDefaultLibrary];
	
	if (!_shaderLibrary) {
		NSLog(@"Failed to created shader library");
		assert(0);
		return;
	}
	
	MTLRenderPipelineDescriptor* pipeDesc = [MTLRenderPipelineDescriptor new];
	pipeDesc.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm;
	pipeDesc.vertexFunction = [_shaderLibrary newFunctionWithName:@"vertexShader"];
	pipeDesc.fragmentFunction = [_shaderLibrary newFunctionWithName:@"fragmentShader"];
	
	NSError* err =[NSError new];
	_pipelineState = [_device newRenderPipelineStateWithDescriptor:pipeDesc
		error: &err];
	
	_scene = [Scene instance];
}

- (void)render:(View *)view
{
	MTLRenderPassDescriptor* desc = view.renderPassDescriptor;
	id<MTLCommandBuffer> cmdBuf = [_commandQueue commandBuffer];
	id<MTLRenderCommandEncoder> enc = [cmdBuf renderCommandEncoderWithDescriptor:desc];

	for (size_t i = 0; i < [[Scene instance] meshCount]; i++) {
		const Mesh* m = [[Scene instance] meshes] + i;
		NSUInteger off = m->positionId * sizeof(float[3]);
		NSUInteger offMat = m->materialId * sizeof(Material);
		
		[enc setRenderPipelineState:_pipelineState];
		[enc setVertexBuffer:[[Scene instance] positionsBuffer]
			offset:off atIndex:0];
		[enc setVertexBuffer:[[Scene instance] materialsBuffer]
			offset:offMat atIndex:1];
		[enc setVertexBuffer:[[Scene instance] PVBuffer]
			offset:0 atIndex:2];
		[enc drawPrimitives:MTLPrimitiveTypeTriangle
			vertexStart:0 vertexCount: m->vertexCount];
	}
	
	[enc endEncoding];
	[cmdBuf presentDrawable: view.drawable];
	[cmdBuf commit];
}

@end
