//
//  Mesh.h
//  MetalSimpleScene
//
//  Created by Arno Luebke on 6/30/15.
//  Copyright (c) 2015 Arno Luebke. All rights reserved.
//

#ifndef MetalSimpleScene_Mesh_h
#define MetalSimpleScene_Mesh_h

#import <stdint.h>

typedef struct {
	uint32_t positionId;
	uint32_t materialId;
	uint32_t vertexCount;
} Mesh;

#endif
