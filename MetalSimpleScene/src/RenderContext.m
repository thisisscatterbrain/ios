#import "RenderContext.h"

@implementation RenderContext

+(RenderContext*)instance
{
	static RenderContext* instance = nil;
	static dispatch_once_t onceToken;
		dispatch_once(&onceToken, ^{
			instance = [[self alloc] init];
	});
	return instance;
}

-(void)configure:(View *)view
{
	_device = MTLCreateSystemDefaultDevice();
}

@end