#import "View.h"

@interface Renderer : NSObject <ViewDelegate>
- (void)configure:(View *)view;
@end