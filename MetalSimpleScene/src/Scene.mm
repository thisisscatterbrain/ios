#import "Scene.h"
#import "Mesh.h"
#import "IO.h"
#import "RenderContext.h"
#import "Tranformations.h"
#import <ISR1/ISR1.h>

#define FLOAT3_FROM_ISR1_VEC3(f3, v3)						\
	f3[0] = v3.x;								\
	f3[1] = v3.y;								\
	f3[2] = v3.z;

@implementation Scene
{
	id<MTLDevice>	_device;
	ISR1_Scene	_scene;
	Mesh*		_meshes;
	uint32_t	_meshCount;
	id<MTLBuffer>	_positionsBuffer;
	id<MTLBuffer>	_materialsBuffer;
	id<MTLBuffer>	_PVBuffer;
}

@synthesize meshes = _meshes;
@synthesize meshCount = _meshCount;
@synthesize positionsBuffer = _positionsBuffer;
@synthesize materialsBuffer = _materialsBuffer;
@synthesize PVBuffer = _PVBuffer;

-(instancetype)init
{
	self = [super init];
	
	if (!self)
		return self;
	
	_meshes = NULL;
	_positionsBuffer = nil;
	_device = [[RenderContext instance] device];
	return self;
}

+(Scene*)instance
{
	static Scene* instance = nil;
	static dispatch_once_t onceToken;
		dispatch_once(&onceToken, ^{
			instance = [[self alloc] init];
	});
	return instance;
}

-(void)initCamera
{
	assert(_scene.cameraCount);
	ISR1_Camera* cam = _scene.cameras;
	float pos[3], up[3], cen[3];
	FLOAT3_FROM_ISR1_VEC3(pos, cam->position);
	FLOAT3_FROM_ISR1_VEC3(up, cam->up);
	FLOAT3_FROM_ISR1_VEC3(cen, cam->center);
	
	pos[0] = 0.0;
	pos[1] = 0.0;
	pos[2] = 5.0;

	simd::float4x4 V = MakeLookAt(pos, cen, up);
	simd::float4x4 P = MakePerspectiveFov(60.0, 768.0 / 1024.0, 0.01, 100.0);
	simd::float4x4 PV = P * V;
	_PVBuffer = [_device newBufferWithBytes:&PV length:sizeof(simd::float4x4) options:0];
	assert(_PVBuffer);
}

-(void)initPositions
{
	size_t posBufSz = _scene.positionCount * sizeof(float[3]);
	float* posBuf = (float*)malloc(posBufSz);
	assert(posBuf && posBufSz);

	for (uint32_t i = 0; i < _scene.positionCount; i++) {
		posBuf[i * 3 + 0] = _scene.positions[i].x;
		posBuf[i * 3 + 1] = _scene.positions[i].y;
		posBuf[i * 3 + 2] = _scene.positions[i].z;
	}
	
	_positionsBuffer = [_device newBufferWithBytes:posBuf length:posBufSz
		options:0];
	assert(_positionsBuffer);
	free(posBuf);
}

-(void)initMaterials
{
	size_t matBufSz = sizeof(Material) * _scene.materialCount;
	Material* matBuf = (Material*)malloc(matBufSz);
	assert(matBuf && matBufSz);
	
	for (size_t i = 0; i < _scene.materialCount; i++) {
		matBuf[i].diffuse[0] = _scene.materials[i].diffuse.x;
		matBuf[i].diffuse[1] = _scene.materials[i].diffuse.y;
		matBuf[i].diffuse[2] = _scene.materials[i].diffuse.z;
	}

	_materialsBuffer = [_device newBufferWithBytes:matBuf length:matBufSz
		options:0];
	assert(_materialsBuffer);
	free(matBuf);
}

-(void)initSceneEntities
{
	_meshes = (Mesh*)calloc(_scene.meshCount, sizeof(*_meshes));
	assert(_meshes);
	_meshCount = _scene.meshCount;
	
	for (uint32_t i = 0; i < _scene.meshCount; i++) {
		ISR1_Mesh* m = _scene.meshs + i;
		_meshes[i].positionId = m->positionsId;
		_meshes[i].materialId = m->materialId;
		_meshes[i].vertexCount = m->vertexCount;
	}	
}

-(void)load:(NSString*)filename
{
	NSString* full = MakeCompleteFilenameForResource(filename);
	assert(ISR1_SUCCESS == ISR1_SceneLoad(&_scene, [full UTF8String]));
	[self initCamera];
	[self initMaterials];
	[self initPositions];
	[self initSceneEntities];
}

-(void)unload
{
	ISR1_SceneDeinit(&_scene);
	free(_meshes);
}

-(void)dealloc
{
	[self unload];
}

@end