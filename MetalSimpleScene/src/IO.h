#ifndef MetalSimpleScene_IO_h
#define MetalSimpleScene_IO_h

#import <Foundation/Foundation.h>

// Builds the complete string needed to load a file identified by filename from
// the Documents directory.
NSString* MakeCompleteFilename(NSString* filename);
NSString* MakeCompleteFilenameForResource(NSString* filename);

#endif
