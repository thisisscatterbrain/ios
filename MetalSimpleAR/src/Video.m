#import "Video.h"
#import "RenderContext.h"

@implementation Video
{
	AVCaptureSession*	_session;
	AVCaptureDevice*	_backCamera;
	AVCaptureDeviceInput*	_input;
	id<MTLDevice>		_device;
	CVMetalTextureCacheRef	_textureCache;

}



+(Video*)instance
{
	static Video* instance = nil;
	static dispatch_once_t onceToken;
		dispatch_once(&onceToken, ^{
			instance = [[self alloc] init];
	});
	return instance;
}

-(instancetype)init
{
	self = [super init];
	
	if (!self)
		return self;
	_device = [[RenderContext instance] device];
	[self initVideo];
	return self;
}

-(void)initVideo
{
	// TODO: this should not be hardcoded!
	_width	= 480;
	_height = 360;
	_frameBuffer = calloc(_width * _height * 4, sizeof(uint8_t));

	// Why flush not created, initialized texture cache? (Note: Code copied
	// from samples).
	CVMetalTextureCacheFlush(_textureCache, 0);
	CVReturn err = CVMetalTextureCacheCreate(
		kCFAllocatorDefault, NULL, _device, NULL, &_textureCache);
	assert(!err);

	_session = [[AVCaptureSession alloc] init];
	assert(_session);
	[_session beginConfiguration];
	[_session setSessionPreset:AVCaptureSessionPresetMedium];
	NSArray* devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
	
	_backCamera = nil;
	
	for (AVCaptureDevice* device in devices) {
		if ([device position] == AVCaptureDevicePositionBack) {
			_backCamera = device;
		}
	}
	

	assert(_backCamera);
	
	NSError *error;
    	AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput
		deviceInputWithDevice:_backCamera error:&error];
	assert(error == nil);
	[_session addInput:deviceInput];

	AVCaptureVideoDataOutput* dataOutput = [[AVCaptureVideoDataOutput alloc] init];
	assert(dataOutput);
	[dataOutput setAlwaysDiscardsLateVideoFrames:YES];
	[dataOutput setVideoSettings:[NSDictionary dictionaryWithObject:
		[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
		forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
	[dataOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
	[_session addOutput:dataOutput];
	[_session commitConfiguration];
	[_session startRunning];
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput
	didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
	fromConnection:(AVCaptureConnection *)connection
{
	CVImageBufferRef imgBuf = CMSampleBufferGetImageBuffer(sampleBuffer);
	assert(imgBuf);
	
	// Copy frame data.
	CVPixelBufferLockBaseAddress(imgBuf, 0);
	uint8_t* baseAddress	= (uint8_t *)CVPixelBufferGetBaseAddress(imgBuf);
	size_t bytesPerRow	= CVPixelBufferGetBytesPerRow(imgBuf);
	size_t width		= CVPixelBufferGetWidth(imgBuf);
	size_t height		= CVPixelBufferGetHeight(imgBuf);
	assert(bytesPerRow == _width * 4);
	assert(width == _width && height == _height);
	memcpy(_frameBuffer, baseAddress, width * height * 4);
	CVPixelBufferUnlockBaseAddress(imgBuf, 0);
	
	// Prepare metal texture.
	CVMetalTextureRef texRef;
	CVReturn error = CVMetalTextureCacheCreateTextureFromImage(
		kCFAllocatorDefault, _textureCache, imgBuf, NULL,
		MTLPixelFormatBGRA8Unorm, width, height, 0, &texRef);
	assert(!error);
	_texture = CVMetalTextureGetTexture(texRef);
	CVBufferRelease(texRef);
}

-(void)dealloc
{
	free(_frameBuffer);
}

@end