#ifndef MetalSimpleAR_Tracker_h
#define MetalSimpleAR_Tracker_h

#import <Foundation/Foundation.h>

@interface Tracker : NSObject
+(Tracker*)instance;
-(void)update;
-(bool)hasPose;
-(const float*)getView;
-(const float*)getProjection;
@end

#endif
