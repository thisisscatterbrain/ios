//
//  RenderVideo.metal
//  MetalSimpleAR
//
//  Created by Arno Luebke on 7/2/15.
//  Copyright (c) 2015 Arno Luebke. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;


struct VertexOut {
	float4 position [[position]];
	float2 uv	[[user(uv)]];
};

vertex VertexOut RenderVideoVertexShader(
	constant float2* positions [[buffer(0)]],
	unsigned int vid [[vertex_id]]
)
{
	VertexOut out;
	float2 p = positions[vid];
	out.position = float4(p, 1.0, 1.0);
	out.uv.x = 1.0 - (p.y * 0.5 + 0.5);
	out.uv.y = 1.0 - (p.x * 0.5 + 0.5);
	return out;
}

fragment float4 RenderVideoFragmentShader(
	VertexOut in [[stage_in]],
	texture2d<float> tex [[texture(0)]]
)
{
	constexpr sampler s(filter::linear);
	return tex.sample(s, in.uv);
}
