#import "Tracker.h"
#import "IO.h"
#import "Video.h"
#import <ART/ART.h>
#import <sys/utsname.h>

NSString* GetDeviceName()
{
    struct utsname systemInfo;
    uname(&systemInfo);

    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

@implementation Tracker
{
	Video*		_video;
	const float*	_view;
	const float*	_projection;
	bool		_hasPose;
	int32_t		_pattId;
}

+(Tracker*)instance
{
	static Tracker* instance = nil;
	static dispatch_once_t onceToken;
		dispatch_once(&onceToken, ^{
			instance = [[self alloc] init];
	});
	return instance;
}

+(const char*)getParameterFilename
{
	NSString* devName = GetDeviceName();
	// TODO: only the test iPad is supported currently!
	assert(NSOrderedSame == [devName compare:@"iPad5,3"]);
	NSString* full = MakeCompleteFilenameForResource(@"iPad2Rear640x480.dat");
	assert(full);
	return [full UTF8String];
}

-(instancetype)init
{
	self = [super self];
	
	if (!self)
		return self;
	
	_video = [Video instance];


	const char* pfn = [Tracker getParameterFilename];
	art_ConfigSetCameraFile(pfn);
	art_ConfigSetResolution(_video.width, _video.height);
	art_Init();
	art_GetProjection(&_projection);
	NSString* patternFilename = MakeCompleteFilenameForResource(@"kanji.patt");
	_pattId = art_LoadPattern([patternFilename UTF8String], 40.0);
	
	return self;
}

-(void)update
{
	art_Detect(_video.frameBuffer);
	_hasPose = art_GetView(&_view, _pattId);
}

-(bool)hasPose
{
	return _hasPose;
}

-(const float*)getView
{
	return _view;
}

-(const float*)getProjection
{
	return _projection;
}

@end














