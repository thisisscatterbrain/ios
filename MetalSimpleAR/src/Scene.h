#ifndef MetalSimpleScene_Scene_h
#define MetalSimpleScene_Scene_h

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>
#import "Mesh.h"
#import "Material.h"

@interface Scene : NSObject
@property(nonatomic, readonly) id<MTLBuffer>	positionsBuffer;
@property(nonatomic, readonly) id<MTLBuffer>	materialsBuffer;
@property(nonatomic, readonly) id<MTLBuffer>	PVBuffer;
@property(nonatomic, readonly) const Mesh*	meshes;
@property(nonatomic, readonly) uint32_t		meshCount;

+(Scene*)instance;
-(void)load:(NSString*) filename;
-(void)unload;
@end

#endif
