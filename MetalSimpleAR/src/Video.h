#ifndef MetalSimpleAR_Video_h
#define MetalSimpleAR_Video_h

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreMedia/CoreMedia.h>


@interface Video : NSObject<AVCaptureVideoDataOutputSampleBufferDelegate>
@property(readonly, nonatomic) uint32_t		width;
@property(readonly, nonatomic) uint32_t		height;
@property(readonly, nonatomic) id<MTLTexture>	texture;
@property(readonly, nonatomic) uint8_t*		frameBuffer;
+ (Video*)instance;
@end

#endif
