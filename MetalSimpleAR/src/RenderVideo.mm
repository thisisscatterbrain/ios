#import "RenderVideo.h"
#import "View.h"
#import "RenderContext.h"
#import "Video.h"

@implementation RenderVideo
{
	id<MTLDevice>			_device;
	id<MTLCommandQueue>		_queue;
	id<MTLRenderPipelineState>	_state;
	id<MTLLibrary>			_shaderLibrary;
	id<MTLBuffer>			_positions;
	MTLRenderPassDescriptor*	_renderPassDesc;
	Video*				_video;
}

+(RenderVideo*)instance
{
	static RenderVideo* instance = nil;
	static dispatch_once_t onceToken;
		dispatch_once(&onceToken, ^{
			instance = [[self alloc] init];
	});
	return instance;
}

-(instancetype)init
{
	self = [super init];
	
	if (!self)
		return self;

	_device = [[RenderContext instance] device];
	_queue = [[RenderContext instance] queue];
	_video = [Video instance];
	_shaderLibrary = [_device newDefaultLibrary];
	_renderPassDesc = [MTLRenderPassDescriptor renderPassDescriptor];
	_renderPassDesc.colorAttachments[0].clearColor =
		MTLClearColorMake(1.0, 1.0, 1.0, 1.0);
	_renderPassDesc.colorAttachments[0].loadAction = MTLLoadActionClear;
	
	MTLRenderPipelineDescriptor* pipeDesc =
		[MTLRenderPipelineDescriptor new];
	pipeDesc.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm;
	pipeDesc.vertexFunction = [_shaderLibrary newFunctionWithName:@"RenderVideoVertexShader"];
	pipeDesc.fragmentFunction = [_shaderLibrary newFunctionWithName:@"RenderVideoFragmentShader"];
	NSError* err;
	_state = [_device newRenderPipelineStateWithDescriptor:pipeDesc
		error: &err];
	assert(nil == err);
	
	static const float positions[] {
			-1.0, -1.0,
			+1.0, -1.0,
			+1.0, +1.0,
			-1.0, -1.0,
			+1.0, +1.0,
			-1.0, +1.0
		};
	
	_positions = [_device newBufferWithBytes:positions
		length:sizeof(positions) options:0];
	return self;
}

-(void)updateRenderPassDescriptor:(id<MTLTexture>)texture
{
	_renderPassDesc.colorAttachments[0].texture = texture;
}

-(void)render:(View*)view
{
	[self updateRenderPassDescriptor:view.drawable.texture];

	id<MTLCommandBuffer> cmdBuf = [_queue commandBuffer];
	id<MTLRenderCommandEncoder> enc =
		[cmdBuf renderCommandEncoderWithDescriptor:_renderPassDesc];
	[enc setRenderPipelineState:_state];
	[enc setVertexBuffer:_positions offset:0 atIndex:0];
	[enc setFragmentTexture:_video.texture atIndex:0];
	[enc drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:6];
	[enc endEncoding];
	[cmdBuf presentDrawable: view.drawable];
	[cmdBuf commit];
}

@end