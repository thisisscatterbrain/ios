#import "RenderContext.h"

@implementation RenderContext

+(RenderContext*)instance
{
	static RenderContext* instance = nil;
	static dispatch_once_t onceToken;
		dispatch_once(&onceToken, ^{
			instance = [[self alloc] init];
	});
	return instance;
}

-(instancetype)init
{
	self = [super init];
	
	if (!self)
		return self;

	_device = MTLCreateSystemDefaultDevice();
	_queue = [_device newCommandQueue];
	return self;
}

@end