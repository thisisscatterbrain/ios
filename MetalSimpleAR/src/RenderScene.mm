#import "RenderScene.h"
#import "RenderContext.h"
#import "Scene.h"
#import "Tracker.h"

@implementation RenderScene
{
	id<MTLDevice>			_device;
	id<MTLCommandQueue>		_queue;
	id<MTLLibrary>			_shaderLibrary;
	id<MTLRenderPipelineState>	_state;
	MTLRenderPassDescriptor*	_renderPassDesc;
	id<MTLBuffer>			_positions;
	Scene*				_scene;
	Tracker*			_tracker;
}

+(RenderScene*)instance
{
	static RenderScene* instance = nil;
	static dispatch_once_t onceToken;
		dispatch_once(&onceToken, ^{
			instance = [[self alloc] init];
	});
	return instance;
}

-(instancetype)init
{
	self = [super init];
	
	if (!self)
		return self;
	
	_device = [[RenderContext instance] device];
	_queue = [[RenderContext instance] queue];
	_shaderLibrary = [_device newDefaultLibrary];
	assert(_shaderLibrary);

	MTLRenderPipelineDescriptor* pipeDesc = [MTLRenderPipelineDescriptor new];
	pipeDesc.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm;
	pipeDesc.vertexFunction = [_shaderLibrary newFunctionWithName:@"RenderSceneVertexShader"];
	pipeDesc.fragmentFunction = [_shaderLibrary newFunctionWithName:@"RenderSceneFragmentShader"];
	
	NSError* err =[NSError new];
	_state = [_device newRenderPipelineStateWithDescriptor:pipeDesc
		error: &err];
	_renderPassDesc = [MTLRenderPassDescriptor renderPassDescriptor];
	_renderPassDesc.colorAttachments[0].clearColor =
		MTLClearColorMake(1.0, 1.0, 1.0, 1.0);
	_renderPassDesc.colorAttachments[0].loadAction = MTLLoadActionLoad;	
	_scene = [Scene instance];
	_tracker = [Tracker instance];
	return self;
}

-(void)updateRenderPassDescriptor:(id<MTLTexture>)texture
{
	_renderPassDesc.colorAttachments[0].texture = texture;
}

-(void)render:(View*)view
{
	[self updateRenderPassDescriptor:view.drawable.texture];

	id<MTLCommandBuffer> cmdBuf = [_queue commandBuffer];
	id<MTLRenderCommandEncoder> enc =
		[cmdBuf renderCommandEncoderWithDescriptor:_renderPassDesc];

	for (size_t i = 0; i < [[Scene instance] meshCount]; i++) {
		const Mesh* m = [[Scene instance] meshes] + i;
		NSUInteger off = m->positionId * sizeof(float[3]);
		NSUInteger offMat = m->materialId * sizeof(Material);
		
		[enc setRenderPipelineState:_state];
		[enc setVertexBuffer:[[Scene instance] positionsBuffer]
			offset:off atIndex:0];
		[enc setVertexBuffer:[[Scene instance] materialsBuffer]
			offset:offMat atIndex:1];
		[enc setVertexBuffer:[[Scene instance] PVBuffer]
			offset:0 atIndex:2];
		[enc drawPrimitives:MTLPrimitiveTypeTriangle
			vertexStart:0 vertexCount: m->vertexCount];
	}
	
	[enc endEncoding];
	[cmdBuf presentDrawable: view.drawable];
	[cmdBuf commit];
}

@end