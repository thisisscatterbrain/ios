#ifndef MetalSimpleAR_RenderScene_h
#define MetalSimpleAR_RenderScene_h

#import "View.h"
#import <Foundation/Foundation.h>

@interface RenderScene : NSObject
+(RenderScene*)instance;
-(void)render:(View*)view;
@end

#endif
