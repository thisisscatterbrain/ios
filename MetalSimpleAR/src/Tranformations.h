#ifndef MetalSimpleScene_Tranformations_h
#define MetalSimpleScene_Tranformations_h

#import <simd/simd.h>
#import <Foundation/Foundation.h>

#define RADIANS_FROM_DEGREES(deg) (deg) / 180.0 * M_PI

simd::float4x4 MakeLookAt(const simd::float3& eye, const simd::float3& center,
	const simd::float3& up);
simd::float4x4 MakeLookAt(const float * const pEye, const float * const pCenter,
	const float * const pUp);
simd::float4x4 MakePerspectiveFov(const float& fovy, const float& aspect,
	const float& near, const float& far);

#endif
