//
//  View.m
//  SpinningCube
//
//  Created by Arno Luebke on 6/23/15.
//  Copyright (c) 2015 Arno Luebke. All rights reserved.
//

#import "View.h"
#import "RenderContext.h"

@implementation View
{
    __weak CAMetalLayer *_metalLayer;
}
@synthesize drawable = _drawable;

- (void)initCommon
{
	_metalLayer = (CAMetalLayer *)self.layer;
	_metalLayer.device = [RenderContext instance].device;
	_metalLayer.pixelFormat = MTLPixelFormatBGRA8Unorm;
}

+ (id)layerClass
{
	return [CAMetalLayer class];
}

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];

	if (self)
		[self initCommon];
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];

	if (self)
		[self initCommon];
	return self;
}


// Gets the drawable for the current frame.
- (id <CAMetalDrawable>)drawable
{
	if (_drawable == nil)
		_drawable = [_metalLayer nextDrawable];
	return _drawable;
}

- (void)display
{
	[self.delegate render:self];
	
	// Make sure to get the next drawable for the following frame (see
	// "drawable" implementation).
	_drawable = nil;
}

@end