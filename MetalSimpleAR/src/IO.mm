#import <Foundation/Foundation.h>

NSString* MakeCompleteFilename(NSString* filename)
{
	NSArray  *documentDirList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [NSString stringWithFormat:@"%@/%@", [documentDirList objectAtIndex:0], filename];
}

NSString* MakeCompleteFilenameForResource(NSString* filename)
{
	NSString* suf = [filename pathExtension];
	NSString* n = [filename stringByDeletingPathExtension];
	return [[NSBundle mainBundle] pathForResource:n	ofType:suf];
}