#import "View.h"

@interface Renderer : NSObject <ViewDelegate>
+(Renderer*)instance;
-(void)configure:(View *)view;
@end