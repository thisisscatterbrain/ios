#ifndef MetalSimpleAR_RenderVideo_h
#define MetalSimpleAR_RenderVideo_h

#import "View.h"
#import <Foundation/Foundation.h>

@interface RenderVideo : NSObject
+(RenderVideo*)instance;
-(void)render:(View*)view;
@end

#endif
