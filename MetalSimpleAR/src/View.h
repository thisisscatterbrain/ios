//
//  View.h
//  SpinningCube
//
//  Created by Arno Luebke on 6/23/15.
//  Copyright (c) 2015 Arno Luebke. All rights reserved.
//

#import <QuartzCore/CAMetalLayer.h>
#import <Metal/Metal.h>
#import <UIKit/UIKit.h>

@protocol ViewDelegate;

@interface View : UIView
@property (nonatomic, weak) id<ViewDelegate>			delegate;
@property (nonatomic, readonly) id <CAMetalDrawable>		drawable;

- (void)display;
@end

@protocol ViewDelegate <NSObject>
@required
- (void)render:(View *)view;
@end
