#include <metal_stdlib>
using namespace metal;

struct Material {
	packed_float3 diffuse;
};

struct VertexOut {
	float4 position [[position]];
	float3 color [[user(color)]];
};

vertex VertexOut RenderSceneVertexShader(
	constant packed_float3*		positions [[buffer(0)]],
	constant Material&		material [[buffer(1)]],
	constant float4x4&		PV [[buffer(2)]],
	unsigned int			vertexId [[vertex_id]]
)
{
	VertexOut out;
	out.position = PV * float4(positions[vertexId], 1.0);
	out.color = material.diffuse;
	return out;
}

fragment float4 RenderSceneFragmentShader(
	VertexOut interpolated [[stage_in]])
{
	return float4(interpolated.color, 1.0);
}
