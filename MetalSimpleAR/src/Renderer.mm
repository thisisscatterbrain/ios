#import "Renderer.h"
#import "RenderContext.h"
#import "RenderVideo.h"
#import "RenderScene.h"
#import "Scene.h"
#import <simd/simd.h>

@implementation Renderer
{
	RenderVideo*			_renderVideo;
	RenderScene*			_renderScene;
	Scene*				_scene;
}

+(Renderer*)instance
{
	static Renderer* instance = nil;
	static dispatch_once_t onceToken;
		dispatch_once(&onceToken, ^{
			instance = [[self alloc] init];
	});
	return instance;
}

-(instancetype)init
{
	self = [super init];
	
	if (!self)
		return self;
	
	_renderVideo = [RenderVideo instance];
	_renderScene = [RenderScene instance];
	return self;
}

- (void)render:(View *)view
{
	[_renderVideo render:view];
	[_renderScene render:view];
}

@end
