#ifndef MetalSimpleScene_RenderContext_h
#define MetalSimpleScene_RenderContext_h

#import <Foundation/Foundation.h>
#import "View.h"

// Shared information when rendering with Metal.
@interface RenderContext : NSObject
@property(nonatomic, readonly) id<MTLDevice>		device;
@property(nonatomic, readonly) id<MTLCommandQueue>	queue;
+(RenderContext*)instance;
@end

#endif
